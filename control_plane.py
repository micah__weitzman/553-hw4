# Name: Micah Weitzman
# PennKey: wmicah


import argparse
from collections import OrderedDict
import json
import os
import struct
import sys
import threading
from time import sleep

sys.path.append("utils")
import bmv2
import helper
from convert import *


def SendDistanceVector(router, p4info_helper, myIPs, distanceVector, dvLock):
    # TODO

    """
    Example of how to send a message to the data plane:
    # dvLock.acquire()
    payload = p4info_helper.buildRoutingPayload(ip, distanceVector)
    dvLock.release()
    router.SendPacketOut(payload)
    """

    while True:
        # periodically send updates to neighbors
        sleep(10)
        # print(distanceVector)
        # sys.stdout.flush()
        for i in range(len(myIPs)):
            dvLock.acquire()  
            payload = p4info_helper.buildRoutingPayload(myIPs[i]["ip"], distanceVector)
            dvLock.release()
            router.SendPacketOut(payload)

        # periodically send updates to neighbors

def RunControlPlane(router, config, p4info_helper):
    # TODO

    distanceVector = []
    dvLock = threading.Lock()
    myIPs = config
    """
    Example of how to wait for multiple digest types:
    digest_request1 = p4info_helper.buildDigestConfig("my_digest_1_t")
    digest_request2 = p4info_helper.buildDigestConfig("my_digest_2_t")
    response = router.GetDigest([digest_request1, digest_request2])
    if response.name == "my_digest_1_t":
        ...
    elif response.name == "my_digest_2_t":
        ...
    """
    
    """
    Example of how to parse the packed bytes of the routing update data
    data = digest.data[0].struct.members[2].bitstring
    # for each index
    prefix, length, cost = struct.unpack_from('!IBB', data, index)
    prefix = decodeIPv4(struct.pack('!I', prefix))
    """
    neighbor_port = []
    cost_map = {}
    local_ip = []
    next_hop = {}
    seen_base = []
    arp_port = []

    for item in myIPs:
        local_ip += item["ip"]
        distanceVector += [[item["ip"], item["prefix_len"], 0]]
        cost_map[item["ip"]] = 0
        # add initial tables 
    add_initial_configs(router, p4info_helper, myIPs, seen_base) 

    arp_digest = p4info_helper.buildDigestConfig("arp_digest_t")
    routing_digest = p4info_helper.buildDigestConfig("routing_digest_t")


    # Start up the update thread.  We need this in its own thread as GetDigest
    # blocks indefinitely.  Just make sure to properly lock data before access.
    update_thread = threading.Thread(target=SendDistanceVector,
                                     args=(router, p4info_helper, myIPs,
                                           distanceVector, dvLock))
    update_thread.start()
    
    while 1:
        # dvLock.acquire()
        response = router.GetDigest([arp_digest, routing_digest])
        
        if response.name == "arp_digest_t":
            ip = decodeIPv4(response.digest.data[0].struct.members[0].bitstring)
            port = decodeNum(response.digest.data[0].struct.members[1].bitstring)
            mac = decodeMac(response.digest.data[0].struct.members[2].bitstring)
            
            
            for local in myIPs:
                if local["port"] == port:
                    # dvLock.acquire()
                    # distanceVector += [[ip, local["prefix_len"], 1]]
                    # dvLock.release()
                    cost_map[ip] = 1
                    next_hop[ip] = ip
                    # add new entry to handOutgoingEthernet
                    base_ip = getBaseIPv4(n["ip"], n["prefix_len"])
                    table_entry = p4info_helper.buildTableEntry(
                        table_name = "cis553Ingress.tiHandleIpv4", 
                        match_fields = {"hdr.ipv4.dstAddr": [base_ip, n["prefix_len"]]},
                        action_name = "cis553Ingress.aiSetNextHop", 
                        action_params = {"next_hop": ip})
                    router.UpdateTableEntry(table_entry)
                    table_entry = p4info_helper.buildTableEntry(
                        table_name = "cis553Ingress.tiHandleOutgoingEthernet", 
                        match_fields = {"metadata.next_hop": local["ip"]},
                        action_name = "cis553Ingress.aiForwardNextHop", 
                        action_params = {"dstAddr": mac, 
                                        "srcAddr": local["mac"], 
                                        "port": port})
                    # if ip in arp_port or ip in local_ip:
                    router.UpdateTableEntry(table_entry)
                    table_entry = p4info_helper.buildTableEntry(
                        table_name = "cis553Ingress.tiHandleOutgoingEthernet", 
                        match_fields = {"metadata.next_hop": ip},
                        action_name = "cis553Ingress.aiForwardNextHop", 
                        action_params = {"dstAddr": mac, 
                                        "srcAddr": local["mac"], 
                                        "port": port})
                    if ip in arp_port or ip in local_ip:
                        router.UpdateTableEntry(table_entry)
                    else:
                        router.WriteTableEntry(table_entry)
                        local_ip += [ip]
                    break 

        elif response.name == "routing_digest_t":
            port = decodeNum(response.digest.data[0].struct.members[0].bitstring)
            ip = decodeIPv4(response.digest.data[0].struct.members[1].bitstring)
            dig_len = decodeNum(response.digest.data[0].struct.members[2].bitstring)
            data = response.digest.data[0].struct.members[3].bitstring

            # if ip not in cost_map:
            #     cost_map[ip] = cost + 1
            #     dvLock.acquire()
            #     distanceVector += [[ip, 32, 1]]
            #     dvLock.release()
            #     next_hop[ip] = ip
            #     update_next_hop(router, p4info_helper, prefix, length, ip)

            for n in myIPs:
                if n["port"] == port and ip not in arp_port: 
                    table_entry = p4info_helper.buildTableEntry(
                        table_name = "cis553Ingress.tiHandleOutgoingEthernet",
                        match_fields = {"metadata.next_hop" : ip},
                        action_name = "cis553Ingress.sendARP", 
                        action_params = {"srcAddr": n["mac"],
                                        "srcIP": n["ip"],
                                        "port": port}
                    ) # broadcast 
                    router.WriteTableEntry(table_entry)
                    arp_port += [ip]

            # sort through list 
            for i in range(dig_len):
                prefix, length, cost = struct.unpack_from('!IBB', data, i*6)
                prefix = decodeIPv4(struct.pack('!I', prefix))
                # getBaseIPv4()
                # print(prefix)
                # sys.stdout.flush()

                # add new entry 
                # if prefix not in local_ip and prefix not in cost_map:
                if prefix not in cost_map:
                    # print("cost_map")
                    # print(cost_map, prefix)
                    # sys.stdout.flush()
                    cost_map[prefix] = cost + 1
                    dvLock.acquire()
                    distanceVector += [[prefix, length, cost + 1]]
                    dvLock.release()
                    next_hop[prefix] = ip
                    update_next_hop(router, p4info_helper, prefix, length, ip, seen_base)

                # update old entry if less
                elif cost + 1 < cost_map[prefix]: # if we found shorter route
                    dvLock.acquire()
                    for i in distanceVector: # get rid of old distance vect
                        if i[0] == prefix:
                            distanceVector.remove(i)
                    # update new values 
                    cost_map[prefix] = cost + 1  
                    distanceVector += [[prefix, length, cost + 1]]
                    next_hop[prefix] = ip 
                    dvLock.release()
                    update_next_hop(router, p4info_helper, prefix, length, ip, seen_base)
        # dvLock.release()
        sleep(1)

    router.shutdown()

def update_next_hop(router, p4info_helper, prefix, length, next_hop, seen_base):
    base_ip = getBaseIPv4(prefix, length)
    table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiHandleIpv4", 
            match_fields = {"hdr.ipv4.dstAddr": [base_ip, length]},
            action_name = "cis553Ingress.aiSetNextHop", 
            action_params = {"next_hop": next_hop})

    if (base_ip, length) not in seen_base:
        router.WriteTableEntry(table_entry)
        seen_base += [(base_ip, length)]
    else:
        router.UpdateTableEntry(table_entry)
    


def add_initial_configs(router, p4info_helper, myIPs, seen_base):
    for n in myIPs:
        # OUTGOING ARP 
        table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiHandleIncomingArpReqest", 
            match_fields = {"standard_metadata.ingress_port": n["port"]},
            action_name = "cis553Ingress.aiARPdigest", 
            action_params = {"src_addr" :n["mac"], 
                    "src_ip":n["ip"]}
        )
        router.WriteTableEntry(table_entry)

        # OUTGOING ROUTING 
        table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiHandleOutgoingRouting", 
            match_fields = {"hdr.distance_vec.src": n["ip"]},
            action_name = "cis553Ingress.forwardNeighbors", 
            action_params = {"port" : n["port"], 
                    "srcAddr" : n["mac"]}
        )
        router.WriteTableEntry(table_entry)

        # FORME (incoming)
        table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiHandleIncomingEthernet", 
            match_fields = {"hdr.ethernet.dstAddr" : n["mac"],
                            "standard_metadata.ingress_port":n["port"]},
            action_name = "cis553Ingress.aiForMe", 
            action_params = {}
        )
        router.WriteTableEntry(table_entry)
        table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiHandleIncomingEthernet", 
            match_fields = {"hdr.ethernet.dstAddr" : "FF:FF:FF:FF:FF:FF",
                            "standard_metadata.ingress_port":n["port"]},
            action_name = "cis553Ingress.aiForMe", 
            action_params = {}
        ) # broadcast 
        router.WriteTableEntry(table_entry)

        base_ip = getBaseIPv4(n["ip"], n["prefix_len"])
        table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiHandleIpv4", 
            match_fields = {"hdr.ipv4.dstAddr": [base_ip, n["prefix_len"]]},
            action_name = "cis553Ingress.aiSetNextHop", 
            action_params = {"next_hop": n["ip"]})
        router.WriteTableEntry(table_entry)
        seen_base += [(base_ip,  n["prefix_len"])]

        table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiHandleOutgoingEthernet",
            match_fields = {"metadata.next_hop" : n["ip"]},
            action_name = "cis553Ingress.sendARP2", 
            action_params = {"srcAddr": n["mac"],
                            "srcIP": n["ip"],
                            "port": n["port"]}
        )
        router.WriteTableEntry(table_entry)









# Starts a control plane for each switch. Hardcoded for our Mininet topology.
def ConfigureNetwork(p4info_file = "build/data_plane.p4info",
                     bmv2_json = "build/data_plane.json",
                     topology_json = "configs/topology.json"):
    p4info_helper = helper.P4InfoHelper(p4info_file)
    with open(topology_json, 'r') as f:
        routers = routers = json.load(f, object_pairs_hook=OrderedDict)['routers']

    threads = []
    port = 50051
    id_num = 0

    for name, config in routers.items():
        config = byteify(config)

        print "Connecting to P4Runtime server on {}...".format(name)
        r = bmv2.Bmv2SwitchConnection(name, "127.0.0.1:" + str(port), id_num)
        r.MasterArbitrationUpdate()
        r.SetForwardingPipelineConfig(p4info = p4info_helper.p4info,
                                      bmv2_json_file_path = bmv2_json)
        t = threading.Thread(target=RunControlPlane,
                             args=(r, config, p4info_helper))
        t.start()
        threads.append(t)

        port += 1
        id_num += 1

    for t in threads:
        t.join()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CIS553 P4Runtime Controller')

    parser.add_argument("-c", '--p4info-file',
                        help="path to P4Runtime protobuf description (text)",
                        type=str, action="store",
                        default="build/data_plane.p4info")
    parser.add_argument("-b", '--bmv2-json',
                        help="path to BMv2 switch description (json)",
                        type=str, action="store",
                        default="build/data_plane.json")
    parser.add_argument("-t", '--topology-json',
                        help="path to the topology configuration (json)",
                        type=str, action="store",
                        default="configs/topology.json")

    args = parser.parse_args()

    if not os.path.exists(args.p4info_file):
        parser.error("File %s does not exist!" % args.p4info_file)
    if not os.path.exists(args.bmv2_json):
        parser.error("File %s does not exist!" % args.bmv2_json)
    if not os.path.exists(args.topology_json):
        parser.error("File %s does not exist!" % args.topology_json)
    
    ConfigureNetwork(args.p4info_file, args.bmv2_json, args.topology_json)
