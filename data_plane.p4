// Name: Micah Weitzman
// PennKey: wmicah 

/* -*- P4_16 -*- */
#include <core.p4>
#include <v1model.p4>

#define CPU_PORT 255


/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/

header ethernet_t {
    bit<48> dstAddr;
    bit<48> srcAddr;
    bit<16> etherType;
}

header ipv4_t {
    bit<4> version; 
    bit<4> ihl;
    bit<8> diffserv; 
    bit<16> totalLen;
    bit<16> identification; 
    bit<3> flags; 
    bit<13> fragOffset;
    bit<8> ttl; 
    bit<8> protocol; 
    bit<16> hdrChecksum; 
    bit<32> srcAddr; 
    bit<32> dstAddr; 
}

header arp_t {
    bit<16> htype;
    bit<16> ptype;
    bit<8> hlen;
    bit<8> plen;
    bit<16> oper;
    bit<48> sha; // sender hardware addr 
    bit<32> spa; // seder protocol addr
    bit<48> tha; // targer hardware addr
    bit<32> tpa; // target protocol addr 
}

// Each element of the distance vector in `data` is:
// bit<32> prefix
// bit<8>  pfx_length
// bit<8>  cost
// Total: 48 bits / 6 bytes (max 42 entries)
header distance_vec_t {
    bit<32>     src;
    bit<16>     length;
    bit<2016>   data;
}


// Overall structure of the packet's headers
struct headers_t {
    ethernet_t      ethernet;
    ipv4_t          ipv4;
    arp_t           arp;
    distance_vec_t  distance_vec;
}


// Declare local variables here
struct local_variables_t {
    bit<1> forMe;
    // TODO
    bit<32> next_hop; 
}


// Digest formats
struct routing_digest_t {
    bit<32>     src;
    bit<16>     length;
    bit<2016>   data;
}

struct arp_digest_t {
    bit<32>     src;
    bit<9>      port;  
    bit<48>     mac; 
}


/*************************************************************************
***********************  P A R S E   P A C K E T *************************
*************************************************************************/

parser cis553Parser(packet_in packet,
                    out headers_t hdr,
                    inout local_variables_t metadata,
                    inout standard_metadata_t standard_metadata) {
    state start {
        transition select(standard_metadata.ingress_port) {
            CPU_PORT: parse_routing;
            default: parse_ethernet;
        }
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            0x0800: parse_ipv4;
            0x0806: parse_arp;
            0x0553: parse_routing;
            default: accept;
        }
    }

    state parse_arp {
        packet.extract(hdr.arp);
        transition accept;
    }

    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition accept;
    }

    state parse_routing {
        packet.extract(hdr.distance_vec);
        transition accept;
    }
}


/*************************************************************************
***********************  I N G R E S S  **********************************
*************************************************************************/

control cis553Ingress(inout headers_t hdr,
                      inout local_variables_t metadata,
                      inout standard_metadata_t standard_metadata) {
    action aiForMe() {
        metadata.forMe = 1;
    }

    action aDrop() {
        mark_to_drop(standard_metadata);
    }

    // Our router is an endpoint on every Ethernet LAN where it is attached.
    // Just like other endpoints, e.g., hosts, it will only listen for L2
    // messages that are addressed to it.
    table tiHandleIncomingEthernet {
        key = {
            hdr.ethernet.dstAddr : exact;
            standard_metadata.ingress_port : exact;
        }
        actions = {
            aiForMe;
            aDrop;
        }

        default_action = aDrop; 
    }

    //************
    //  ROUTING //
    //************
    // add L2 Header 
    action forwardNeighbors(bit<9> port, bit<48> srcAddr) {
        standard_metadata.egress_spec = (bit<9>)port;
        hdr.ethernet.setValid(); 
        hdr.ethernet.dstAddr = 0xFFFFFFFFFFFF; 
        hdr.ethernet.srcAddr = srcAddr; 
        hdr.ethernet.etherType = 0x0553; 
        // hdr.distance_vec.data = 0x68656c6c6f20776f726c64; // Dummy data 
        hdr.distance_vec.setValid(); 
    }
    // Called when we get a routing message from the control plane.  Send it out
    // to all of our neighbors.
    table tiHandleOutgoingRouting {
        // TODO
        key = {
            hdr.distance_vec.src : exact;
        }
        actions = {
            forwardNeighbors; 
        }
    }
    

    //************
    //  IPV4    //
    //************

    // Look up the IP of the next hop on the route using a Longest Prefix Match
    // (lpm) rather than an exact match
    table tiHandleIpv4 {
        key = {
            hdr.ipv4.dstAddr : lpm;
        }
        actions = { 
        }
    }

    action aiForwardNextHop(bit<48> dstAddr, bit<48> srcAddr, bit<9> port) {
        standard_metadata.egress_spec = port;
        hdr.ethernet.srcAddr = srcAddr;
        hdr.ethernet.dstAddr = dstAddr; 
        hdr.arp.setInvalid(); 
    }

    action sendARP() {
        
    }

    // Given an IP, look up the destination MAC address and physical port.
    // This serves the same purpose as the hosts' ARP tables and L2
    // encapsulation.
    table tiHandleOutgoingEthernet {
       key = {
            metadata.next_hop : exact;
        }
        actions = {
            forwardNeighbors;
            sendARP; 
        }
        default_action = sendARP
    }

    // **************
    // INCOMING ARP *
    // **************
    action aiARPdigest(bit<48> src_addr, bit<32> src_ip) {
        // arp_digest_t arp_digest = {hdr.arp.spa,
        //                         standard_metadata.ingress_port, 
        //                         hdr.ethernet.srcAddr};
        // digest(0, arp_digest);

        hdr.ethernet.dstAddr = hdr.ethernet.srcAddr; 
        hdr.ethernet.srcAddr = src_addr; 
        standard_metadata.egress_spec = standard_metadata.ingress_port; 
        hdr.arp.htype = 1;// ethernet 
        hdr.arp.ptype = 0x0800; 
        hdr.arp.hlen = 6; // etherneret size 
        hdr.arp.plen = 4; 
        hdr.arp.oper = 2; // mark reply 

        hdr.arp.tha = hdr.arp.sha; 
        hdr.arp.sha = src_addr;
        hdr.arp.tpa = hdr.arp.spa;  
        hdr.arp.spa = src_ip; 

        hdr.ethernet.setValid();
        hdr.arp.setValid(); 
    }

    // Respond to ARP requests for our IP to tell the requester that packets
    // sent to our IP should be addressed, at Layer-2, to our MAC address.
    table tiHandleIncomingArpReqest {
        // TODO
        key = {
            standard_metadata.ingress_port : exact;
        }

        actions = {
            aiARPdigest; 
        }
        default_action = aiARPdigest; 
    }



    // *********************
    // HANDLE ARP RESPONSE *
    // *********************

    action aiArpResponse(bit<48> dstAddr, bit<48> srcAddr, bit<9> port) {
        arp_digest_t arp_digest = {hdr.arp.spa,
                                standard_metadata.ingress_port, 
                                hdr.ethernet.srcAddr};
        digest(0, arp_digest);

        // hdr.ethernet.dstAddr = dstAddr; 
        // hdr.ethernet.srcAddr = src_addr; 

        // hdr.ether

        // hdr.arp.tha = hdr.arp.sha; 
        // hdr.arp.sha = src_addr;
        // hdr.arp.tpa = hdr.arp.spa;  
        // hdr.arp.spa = src_ip; 

        // hdr.ethernet.setValid();
        // hdr.arp.setValid(); 
    }

    // Handle a response to our prior ARP request.  Adding this new entry to the
    // tiHandleOutgoingEthernet lookup table should probably involve the control
    // plane.
     table tiHandleIncomingArpResponse {
        // TODO
        key = {
            hdr.arp.spa: exact; 
        }
        actions = {
            aiHandleIncomingArpResponse; 
        }
        default_action = aiHandleIncomingArpResponse(); 
    }




    // **********************************
    // INCOMING ROUTING FROM OTHER HOST *
    // **********************************
    action aiIncomingRouting() {
        routing_digest_t routing_digest = {
                    standard_metadata.ingress_port,  
                    hdr.distance_vec.src, 
                    hdr.distance_vec.length, 
                    hdr.distance_vec.data};
        digest(1, routing_digest);
    }

    // Called when the packet has both an Ethernet header and is a routing
    // message.  That means it's from another host and we should send it to the
    // control plane.
    table tiHandleIncomingRouting {
        // TODO
        key = {
            hdr.distance_vec.src: exact; 
        }
        actions = {
            aiIncomingRouting; 
        }
        default_action = aiIncomingRouting(); 
    }

    apply {
        // Check the first header
        if (hdr.ethernet.isValid()) {
            tiHandleIncomingEthernet.apply();
        } else {
            // routing packets from the control plane come raw (no L2 header)
            tiHandleOutgoingRouting.apply();
        }

        // Check the second header if the Ethernet dst is us
        if (metadata.forMe == 0) {
            // Don't do anything with it
        } else if (hdr.ipv4.isValid()) {
            tiHandleIpv4.apply();
            tiHandleOutgoingEthernet.apply();
        } else if (hdr.arp.isValid() && hdr.arp.oper == 1) {
            tiHandleIncomingArpReqest.apply();
        } else if (hdr.arp.isValid() && hdr.arp.oper == 2) {
            tiHandleIncomingArpResponse.apply();
        } else if (hdr.distance_vec.isValid()) {
            tiHandleIncomingRouting.apply();
        } else {
            aDrop();
        }
    }
}


/*************************************************************************
***********************  E G R E S S  ************************************
*************************************************************************/

control cis553Egress(inout headers_t hdr,
                     inout local_variables_t metadata,
                     inout standard_metadata_t standard_metadata) {
    apply { }
}


/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control cis553VerifyChecksum(inout headers_t hdr,
                             inout local_variables_t metadata) {
     apply { }
}


/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   ***************
*************************************************************************/

control cis553ComputeChecksum(inout headers_t hdr,
                              inout local_variables_t metadata) {
    // The switch handles the Ethernet checksum, so we don't need to deal with
    // that, but we do need to deal with the IP checksum!
    apply {
        update_checksum(
            hdr.ipv4.isValid(),
            { hdr.ipv4.version,
              hdr.ipv4.ihl,
              hdr.ipv4.diffserv,
              hdr.ipv4.totalLen,
              hdr.ipv4.identification,
              hdr.ipv4.flags,
              hdr.ipv4.fragOffset,
              hdr.ipv4.ttl,
              hdr.ipv4.protocol,
              hdr.ipv4.srcAddr,
              hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}


/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   ********************
*************************************************************************/

control cis553Deparser(packet_out packet, in headers_t hdr) {
    apply {
        packet.emit(hdr.ethernet);
        packet.emit(hdr.arp);
        packet.emit(hdr.ipv4);
        packet.emit(hdr.distance_vec);
    }
}


/*************************************************************************
***********************  S W I T C H  ************************************
*************************************************************************/

V1Switch(cis553Parser(),
         cis553VerifyChecksum(),
         cis553Ingress(),
         cis553Egress(),
         cis553ComputeChecksum(),
         cis553Deparser()) main;
